# Shock wave reflection over straight wedges

This repository have Shock Wave Reflection case file for openfoam.

## Getting started

リポジトリの一覧ページで右上のダウンロードマークから圧縮形式のファイルをダウンロードして，PC内のOpenFOAM環境で使ってください

## 注意点

- blockMeshの設定で，メッシュが細かい設定になっているファイルがあります．調整してください．
- decomposeParDictの設定でCPUコア数についてはPCのコア数に応じて調整してください．
- Allrunは並列計算しているので，各ファイルの設定を見直してから使ってください．



## 衝撃波前後の物理量を求める際のノートブックの使用方法

### 準備

- PCにpython本体が入っていることを確認してください。
- pythonのサブモジュールであるnumpyがインストールされているのを確認してください
- Jupyter notebookを実行できる環境であることを確認してください。

### 実行

使用するガスに応じて、分子量と比熱比を設定してください。
デフォルトでは空気を入力してあります。

Rは衝撃波前方の、Lは衝撃波背後の物理量を表します。
衝撃波前方の物理量を設定して実行すると、衝撃波背後の物理量が出力される。

